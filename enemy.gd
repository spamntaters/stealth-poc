extends CharacterBody2D

const DETECT_RADIUS = 200
const FOV = 80

@export var speed :float = 120

var player :Node2D
var parent :Node2D
var vision_cone :Node2D
var last_pos :Vector2
var direction :Vector2 = Vector2()
var angle = 0
var draw_color = Color(1, 0, 0, 0)

func _ready() -> void:
	player = get_node("/root/Player")
	parent = get_parent()
	last_pos = global_position
	vision_cone = $Polygon2D
	vision_cone.show()
	set_process(true)

func _process(delta :float) -> void:
	parent.progress += speed*delta
	direction = (global_position - last_pos).normalized()
	last_pos = global_position
	var detectsPlayer = false
	var playerDistance = global_position.distance_to(player.global_position)
	if playerDistance < DETECT_RADIUS:
		var dot_product = direction.dot(player.global_position)
		var angle_to_player = rad_to_deg(acos(dot_product))
		if abs(angle_to_player) < FOV/2:
			detectsPlayer = true
	var opacity = 1 - (playerDistance/DETECT_RADIUS)
	draw_color = Color(1, 0, 0, opacity)
	vision_cone.queue_redraw()
	
	
